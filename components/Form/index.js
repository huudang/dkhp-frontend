import React, { useState } from 'react'
import Axios from 'axios'
import Swal from 'sweetalert2'

const API_URL = 'http://13.229.125.47/auth/local'
const API_URL_DKHP = 'http://13.229.125.47/phieu-dang-kis'

const Form = (props) => {
  const [maMonHoc, setMaMonHoc] = useState('')
  const [optional, setOptional] = useState('')

  const handleSubmit = () => {
    Axios.post(API_URL, {
      identifier: '18520014',
      password: '123456789'
    }).then(({ data }) => {
      const jwt = data.jwt
      const listId = maMonHoc.split('\n')
      Axios.post(API_URL_DKHP,
        {
          listId,
          optional
        },
        {
          headers: {
            Authorization: 'Bearer ' + jwt
          }
        }
      ).then(({ data }) => {
        let timerInterval
        Swal.fire({
          title: 'Đang đăng kí...',
          timer: 5000,
          icon: 'success',
          timerProgressBar: true,
          onBeforeOpen: () => {
            Swal.showLoading()
            timerInterval = setInterval(() => {
              const content = Swal.getContent()
              if (content) {
                const b = content.querySelector('b')
                if (b) {
                  b.textContent = Swal.getTimerLeft()
                }
              }
            }, 100)
          },
          onClose: () => {
            clearInterval(timerInterval)
          }
        })
      })
    })
      .catch(err => {
        if (err) {
          Swal.fire({
            icon: 'error',
            title: 'Đăng kí không thành công',
            text: 'Xin hãy kiểm tra lại'
          })
        }
      })
  }

  return (
    <>
      {/* Contact Form */}
      <div className='container text-center'>
        <header className='u-heading-v8-2 text-center g-width-70x--md mx-auto g-mb-60'>
          <h2 className='u-heading-v8__title text-uppercase g-font-weight-600 g-mb-25'>Đăng kí môn học</h2>
          <p className='lead mb-0'>
            Dán mã lớp bạn muốn đăng kí vào form bên dưới. Hệ thống sẽ trả về kết quả trong giây lát.
          </p>
        </header>
        {/* Contact Form */}
        <div className='row justify-content-center'>
          <div className='col-md-8'>
            <form>
              <div className='row'>
                <div className='col-md-12 form-group g-mb-30'>
                  <textarea className='form-control g-color-gray-dark-v5 g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--focus g-resize-none rounded-0 g-py-13 g-px-15' rows={7} placeholder='Danh sách mã lớp.' onChange={(event) => setMaMonHoc(event.target.value)} />
                </div>
                <div className='col-md-12 form-group g-mb-30'>
                  <input className='form-control g-color-gray-dark-v5 g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--focus rounded-0 g-py-13 g-px-15' type='text' placeholder='Ghi chú thêm cho thầy cô bộ môn' onChange={(event) => setOptional(event.target.value)} />
                </div>
              </div>
              <button className='btn u-btn-primary rounded-0 g-py-12 g-px-20' type='button' onClick={handleSubmit}>Gửi đi</button>
            </form>
          </div>
        </div>
        {/* Contact Form */}
      </div>
      {/* End Contact Form */}
    </>
  )
}

export default Form
