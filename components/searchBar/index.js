import React, { useState } from 'react'
import Axios from 'axios'
import Swal from 'sweetalert2'

const API_URL = 'http://13.229.125.47/mon-hocs'

const SearchBar = (props) => {
  const [id, setId] = useState()
  const [name, setName] = useState('')

  const handleSubmit = () => {
    let url = API_URL + '?'
    if (id.length != 0) url += `id=${id}`
    if (name.length != 0) url += `&tenMonHoc=${name}`
    Axios.get(url)
      .then(({ data }) => {
        console.log(data)
        Swal.fire({
          title: `${data[0].id} - ${data[0].tenMonHoc}`,
          icon: 'info',
          text: `Học phí môn học: ${data[0].hocPhiMonHoc} - Sĩ số: ${data[0].siSo} - Số tiết: ${data[0].soTiet}`,
          footer: `Số tín chỉ: ${data[0].tinChi}`
        })
      })
      .catch(err => {
        console.log(err)
      })
  }

  return (
    <>
      {/* Promo Block */}
      <section className='dzsparallaxer auto-init height-is-based-on-content use-loading' data-options='{direction: &quot;reverse&quot;, settings_mode_oneelement_max_offset: &quot;150&quot;}'>
        {/* Parallax Image */}
        <div className='divimage dzsparallaxer--target w-100 u-bg-overlay g-bg-white-opacity-0_8--after' style={{ height: '120%', backgroundImage: 'url(../../assets/img-temp/1920x800/img3.jpg)' }} />
        {/* End Parallax Image */}
        <div className='container u-bg-overlay__inner text-center g-py-100 g-py-160--md'>
          <h2 className='h1 g-color-gray-dark-v1 text-uppercase g-font-weight-600 g-width-60x--md mx-auto g-mb-30'>Tìm kiếm môn học nhanh !</h2>
          {/* Search Form */}
          <form>
            <div className='row justify-content-center g-mb-20 g-mb-0--lg'>
              <div className='col-lg-4'>
                <div id='form-icon-magnifier' className='input-group u-shadow-v21 g-bg-white rounded g-mb-15'>
                  <div className='input-group-append'>
                    <span className='input-group-text rounded-0 border-0 g-font-size-16 g-color-gray-light-v1'><i className='icon-magnifier g-pos-rel g-top-1 g-px-1' /></span>
                  </div>
                  <input className='form-control form-control-md g-font-size-16 border-0 g-pl-2 g-py-15' type='text' placeholder='Mã môn học' aria-label='Mã môn học' aria-describedby='form-icon-magnifier' onChange={(event) => setId(event.target.value)} />
                </div>
              </div>
              <div className='col-lg-4'>
                <div className='input-group u-shadow-v21 g-bg-white rounded g-mb-15'>
                  <div id='form-icon-location-pin' className='input-group-append'>
                    <span className='input-group-text rounded-0 border-0 g-font-size-16 g-color-gray-light-v1'><i className='icon-location-pin g-pos-rel g-top-1 g-px-1' /></span>
                  </div>
                  <input className='form-control form-control-md g-font-size-16 border-0 g-pl-2 g-py-15' type='text' placeholder='Tên môn học' aria-label='Tên môn học' aria-describedby='form-icon-location-pin' onChange={(event) => setName(event.target.value)} />
                </div>
              </div>
              <div className='col-lg-2'>
                <button className='btn btn-md btn-block u-btn-primary u-shadow-v21 text-uppercase g-py-14' type='button' onClick={handleSubmit}>Tìm kiếm</button>
              </div>
            </div>
          </form>
          {/* End Search Form */}
        </div>
      </section>
      {/* Promo Block */}
    </>
  )
}

export default SearchBar
