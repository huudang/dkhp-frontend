import React, { useEffect, useState } from 'react'
import Swal from 'sweetalert2'
import Axios from 'axios'

const API_URL = 'http://13.229.125.47/sinh-vien-chua-hoan-thanh-hoc-phis'

const PricingInfo = () => {
  // useEffect(() => {
  //   paypal.Buttons({
  //     createOrder: function () {
  //       return axios.post('http://localhost:1337/transactions', {
  //         value: '1',
  //         currency: 'USD'
  //       }).then(function (res) {
  //         return res.data
  //       }).catch(function (error) {
  //         console.log(error);
  //       });
  //     },

  //     onApprove: function (data) {
  //       return axios.put('http://localhost:1337/transactions/capture', {
  //         orderID: data.orderID
  //       }).then(function (res) {
  //         console.log(res)
  //         Swal.fire({
  //           title: 'Transaction success',
  //           text: 'Transaction completed by ' + res.data.payer.name.given_name,
  //           icon: 'success'
  //         })
  //         return res.data
  //       }).catch(function (error) {
  //         Swal.fire({
  //           title: 'Transaction failed',
  //           text: 'Transaction failed with error:', error,
  //           icon: 'warning'
  //         })
  //       })
  //     }
  //   }).render('#paypal-button-container');

  // }, [])
  const [info, setInfo] = useState()

  useEffect(() => {
    Axios.get(API_URL + '?id=5')
      .then(({ data }) => {
        console.log('data: ', data)
        setInfo(data)
      })
      .catch(err => {
        console.log(err)
      })
  }, [])

  const handleOnclick = () => {
    Axios.put(API_URL + '/5', {
      conLai: 0,
      hocPhiDaDong: info[0].hocPhiDangKi
    })
      .then(({ data }) => {
        console.log(data)
      })
    let timerInterval
    Swal.fire({
      title: 'Đang xử lý...',
      timer: 2000,
      icon: 'success',
      timerProgressBar: true,
      onBeforeOpen: () => {
        Swal.showLoading()
        timerInterval = setInterval(() => {
          const content = Swal.getContent()
          if (content) {
            const b = content.querySelector('b')
            if (b) {
              b.textContent = Swal.getTimerLeft()
            }
          }
        }, 100)
      },
      onClose: () => {
        clearInterval(timerInterval)
      }
    })
  }

  if (info) {
    return (
      <>
        {/* Section */}
        <section className='container g-pt-100 g-pb-40'>
          <div className='row'>
            <div className='col-lg-6 align-self-center g-mb-60'>
              <img
                className='img-fluid' src='https://i.imgur.com/4o1obwD.jpg' alt='Image Description'
                style={{ borderRadius: 100, wdisplay: 'block', maxWidth: '400px', maxHeight: '400px', width: 'auto', height: 'auto' }}
              />
            </div>
            <div className='col-lg-6 align-self-center g-mb-60 center'>
              <div className='mb-2'>
                <h2 className='h3 g-color-black text-uppercase mb-2'>THÔNG TIN HỌC PHÍ HỌC KỲ 2</h2>
                <div className='d-inline-block g-width-35 g-height-2 g-bg-blue mb-2' />
              </div>
              <div className='mb-5'>
                <p className='mb-5'>
              Hiện chưa có ghi chú nào
                </p>
                <ul className='list-unstyled g-font-size-13 mb-0'>
                  {/* <li>
                    <i className='d-inline-block g-color-primary mr-2 mb-3 fa fa-check' />
                  Số TC học phí: 20.0 (TC)
                  </li> */}
                  <li>
                    <i className='d-inline-block g-color-primary mr-2 mb-3 fa fa-check' />
                  Học phí phải đóng: {info[0].hocPhiDangKi} (đ)
                  </li>
                  <li>
                    <i className='d-inline-block g-color-primary mr-2 mb-3 fa fa-check' />
                  Học phí nợ trước: 0 (đ)
                  </li>
                  <li>
                    <i className='d-inline-block g-color-primary mr-2 mb-3 fa fa-check' />
                  Học phí đã đóng: {info[0].hocPhiDaDong} (đ)
                  </li>
                  <li>
                    <i className='d-inline-block g-color-primary mr-2 mb-3 fa fa-check' />
                  Học phí còn nợ: {info[0].conLai} (đ)
                  </li>
                </ul>
              </div>
              {/* <a className='btn btn-md u-btn-outline-black g-font-size-default g-rounded-50 g-py-10 mr-2' href='#'>Explore more</a> */}
              <a className='btn btn-md u-btn-yellow g-font-size-default g-rounded-50 g-py-10' onClick={() => handleOnclick()}>Đóng học phí ngay</a>
            </div>
          </div>
        </section>
        {/* End Section */}
      </>
    )
  } else return (<> </>)
}

export default PricingInfo
