import React from 'react'

const Process = () => {
  return (
    <>
      <section className='g-bg-secondary'>
        <div className='container g-pt-100 g-pb-40'>
          <div className='row justify-content-center g-mb-60'>
            <div className='col-lg-7'>
              {/* Heading */}
              <div className='text-center'>
                <h2 className='h3 g-color-black text-uppercase mb-2'>Quy trình đăng kí</h2>
                <div className='d-inline-block g-width-35 g-height-2 g-bg-blue mb-2' />
              </div>
              {/* End Heading */}
            </div>
          </div>
          {/* Static Process */}
          <div className='row'>
            <div className='col-sm-6 col-lg-3 g-mb-60'>
              {/* Static Process */}
              <div className='text-center'>
                <i className='u-dot-line-v1-2 g-brd-transparent--before g-brd-gray-light-v2--after g-mb-20'>
                  <span className='u-dot-line-v1__inner g-bg-white g-bg-primary--before g-brd-gray-light-v2' />
                </i>
                <h3 className='h5 g-color-black mb-20'>Chuẩn bị</h3>
                <p className='mb-0'>Tìm kiếm thông tin và lên danh sách mã lớp dự định học.</p>
              </div>
              {/* End Static Process */}
            </div>
            <div className='col-sm-6 col-lg-3 g-mb-60'>
              {/* Static Process */}
              <div className='text-center'>
                <i className='u-dot-line-v1-2 g-brd-gray-light-v2--before g-brd-gray-light-v2--after g-mb-20'>
                  <span className='u-dot-line-v1__inner g-bg-white g-bg-primary--before g-brd-gray-light-v2' />
                </i>
                <h3 className='h5 g-color-black mb-20'>Đăng kí học phần</h3>
                <p className='mb-0'>Dán danh sách mã lớp vào khung đăng kí và đợi đến khi có thông báo thành công.</p>
              </div>
              {/* End Static Process */}
            </div>
            <div className='col-sm-6 col-lg-3 g-mb-60'>
              {/* Static Process */}
              <div className='text-center'>
                <i className='u-dot-line-v1-2 g-brd-gray-light-v2--before g-brd-gray-light-v2--after g-mb-20'>
                  <span className='u-dot-line-v1__inner g-bg-white g-bg-primary--before g-brd-gray-light-v2' />
                </i>
                <h3 className='h5 g-color-black mb-20'>Xem thông tin học phí</h3>
                <p className='mb-0'>Thông tin được cập nhật theo ngày, vì thế sinh viên chú ý.</p>
              </div>
              {/* End Static Process */}
            </div>
            <div className='col-sm-6 col-lg-3 g-mb-60'>
              {/* Static Process */}
              <div className='text-center'>
                <i className='u-dot-line-v1-2 g-brd-gray-light-v2--before g-brd-transparent--after g-mb-20'>
                  <span className='u-dot-line-v1__inner g-bg-white g-bg-primary--before g-brd-gray-light-v2' />
                </i>
                <h3 className='h5 g-color-black mb-20'>Đóng học phí</h3>
                <p className='mb-0'>Có thể thanh toán qua gửi trực tiếp, gửi qua thẻ và thanh toán online.</p>
              </div>
              {/* End Static Process */}
            </div>
          </div>
          {/* End Static Process */}
        </div>
      </section>
    </>
  )
}

export default Process
