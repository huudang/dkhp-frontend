const NextI18Next = require('next-i18next').default
const numeral = require('numeral')
const _ = require('lodash')
// const numeralLocalesConstants = require('../constants/numeralLocales')

// if (!_.has(numeral, 'locales.my_en')) {
//   numeral.register('locale', 'my_en', numeralLocalesConstants.en)
// }

// if (!_.has(numeral, 'locales.my_vi')) {
//   numeral.register('locale', 'my_vi', numeralLocalesConstants.vi)
// }

const NextI18NextInstance = new NextI18Next({
  defaultLanguage: 'vi',
  otherLanguages: ['en'],
  defaultNS: 'common',
  localePath: 'locales',
  localeSubpaths: {
    vi: 'vi',
    en: 'en'
  },
  browserLanguageDetection: false,

  interpolation: {
    escapeValue: false,
    format: function (value, format, lng) {
      if (!value) return ''

      if (format === 'shortNumberFormatter') {
        numeral.locale('my_' + lng)
        return numeral(value).format('0,0.[0]a')
      }

      if (format === 'numberFormatter') {
        numeral.locale('my_' + lng)
        return numeral(value).format('0,0.[0]')
      }

      return value
    }
  }
})

module.exports = NextI18NextInstance
