import React from 'react'

import SearchBar from '../../components/searchBar'
import Header from '../../components/Header'
import Subject from '../../components/Subject'
import Form from '../../components/Form'

const FormRegister = (props) => {
  return (
    <>
      <Header />
      <SearchBar />
      <Subject />
      <Form />
    </>
  )
}

export default FormRegister
