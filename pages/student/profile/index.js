import React, { useState } from 'react'
import Router from 'next/router'

import Header from '../../../components/Header'

import Profile from '../../../containers/Profile'
import Setting from '../../../containers/SettingProfile'

const profile = (props) => {
  console.log('props:', props)
  const [option, setOption] = useState('Profile')

  const handleDKHP = (event) => {
    event.preventDefault()
    Router.push('/form-register')
  }

  const handlePricing = (event) => {
    event.preventDefault()
    Router.push('/pricing')
  }

  return (
    <>
      <Header />
      <section className='g-mb-100'>
        <div className='container'>
          <div className='row'>
            {/* <!-- Profile Sidebar --> */}
            <div className='col-lg-3 g-mb-50 g-mb-0--lg'>
              {/* <!-- User Image --> */}
              <div className='u-block-hover g-pos-rel'>
                <figure>
                  <img className='img-fluid w-100 u-block-hover__main--zoom-v1' src='https://i.imgur.com/4o1obwD.jpg' alt='Image Description' />
                </figure>

                {/* <!-- Figure Caption --> */}
                <figcaption className='u-block-hover__additional--fade g-bg-black-opacity-0_5 g-pa-30'>
                  <div className='u-block-hover__additional--fade u-block-hover__additional--fade-up g-flex-middle'>
                    {/* <!-- Figure Social Icons --> */}
                    <ul className='list-inline text-center g-flex-middle-item--bottom g-mb-20'>
                      <li className='list-inline-item align-middle g-mx-7'>
                        <a className='u-icon-v1 u-icon-size--md g-color-white' href='#'>
                          <i className='icon-note u-line-icon-pro' />
                        </a>
                      </li>
                      <li className='list-inline-item align-middle g-mx-7'>
                        <a className='u-icon-v1 u-icon-size--md g-color-white' href='#'>
                          <i className='icon-notebook u-line-icon-pro' />
                        </a>
                      </li>
                      <li className='list-inline-item align-middle g-mx-7'>
                        <a className='u-icon-v1 u-icon-size--md g-color-white' href='#'>
                          <i className='icon-settings u-line-icon-pro' />
                        </a>
                      </li>
                    </ul>
                    {/* <!-- End Figure Social Icons --> */}
                  </div>
                </figcaption>
                {/* <!-- End Figure Caption --> */}

                {/* <!-- User Info --> */}
                <span className='g-pos-abs g-top-20 g-left-0'>
                  <a className='btn btn-sm u-btn-blue rounded-0' href='#'>Nguyễn Trần Hữu Đăng</a>
                  <small className='d-block g-bg-black g-color-white g-pa-5'>Sinh viên</small>
                </span>
                {/* <!-- End User Info --> */}
              </div>
              {/* <!-- User Image --> */}

              {/* <!-- Sidebar Navigation --> */}
              <div className='list-group list-group-border-0 g-mb-40'>
                {/* <!-- Profile --> */}
                <a href='#' className='list-group-item justify-content-between' onClick={() => setOption('Profile')}>
                  <span><i className='icon-cursor g-pos-rel g-top-1 g-mr-8' />Bảng điều khiển</span>
                </a>
                {/* <!-- End Profile --> */}

                {/* <!-- Settings --> */}
                <a href='#' className='list-group-item list-group-item-action justify-content-between' onClick={() => setOption('Setting')}>
                  <span><i className='icon-settings g-pos-rel g-top-1 g-mr-8' /> Cài đặt</span>
                  {/* <span className='u-label g-font-size-11 g-bg-cyan g-rounded-20 g-px-8'>3</span> */}
                </a>
                {/* <!-- End Settings --> */}
              </div>
              {/* <!-- End Sidebar Navigation --> */}

            </div>
            {/* <!-- End Profile Sidebar --> */}

            {(option === 'Profile' && <Profile handleDKHP={handleDKHP} handlePricing={handlePricing} />) || <Setting />}
          </div>
        </div>
      </section>
    </>
  )
}

export default profile
